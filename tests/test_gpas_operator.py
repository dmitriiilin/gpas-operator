"""Main gpas_operator testing"""
import json
import os

import pytest

from gpas_operator import NomadListener


def load_json(file: str):
    """Load json object from file"""

    current = os.path.dirname(os.path.realpath(__file__))
    rel_file = f"{current}/data/{file}.json"
    with open(rel_file) as json_file:
        return json.load(json_file)


@pytest.mark.asyncio
async def test_get_allocation_ports():
    """Testing get_allocation_ports()"""
    async def dummy_nomad_req(_, sec=""):
        return load_json(sec), 0

    nomadl = NomadListener(None, None)
    nomadl.nomad_req = dummy_nomad_req

    res = await nomadl.get_allocation_ports(None, "game-configuration")
    assert res == {}

    res = await nomadl.get_allocation_ports(None, "gpas-core")
    assert res['akka'] == 30144

    res = await nomadl.get_allocation_ports(None, "gpas-container")
    assert res['api'] == 20711

    res = await nomadl.get_allocation_ports(None, "gpas-mp")
    assert res['http'] == 2607
