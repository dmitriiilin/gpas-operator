"""PoC for simple Akka downing monitor written in Python"""
import asyncio
import logging
import os
import random
from datetime import datetime, timedelta

import aiohttp
from aiologstash import create_tcp_handler


class DummyLogger:
    """Used in case of absence of Logstash settings"""
    def debug(*kwarg):
        pass


class NomadListener:
    """Listen for Nomad events and react with custom logic"""
    events = {}
    allocs = {}
    client = None
    req_retry = 1
    log = DummyLogger()

    def __init__(self, nomad_srv: str, logstash: (str, str)):
        self.nomad_srv = nomad_srv
        self.logstash = logstash

    async def nomad_req(self, client, sec: str, index: int = None):
        """Process HTTP(s) request to Nomad server and return result and index
        for updating. Use *nomad_server* as address and port of requested Nomad
        server, *sec* as section of API (for example, 'allocations')"""
        payload = {'time': '30s'}
        if index:
            payload['index'] = index
        url = "{}/v1/{}".format(self.nomad_srv, sec)
        async with client.get(url, params=payload) as resp:
            if resp.status == 200:
                index = resp.headers['X-Nomad-Index']
                return await resp.json(), index
            return None, index

    async def get_allocation_ports(self, client, alloc_id: str):
        """Get IP:Port information by allocation id

        return dict of 'port name': 'port number'
         e.g. "akka": 30144
        """
        resp, _ = await self.nomad_req(client,
                                       'allocation/{}'.format(alloc_id))
        tasks = resp['AllocatedResources']['Tasks']
        first_task = list(tasks.keys())[0]
        try:
            first_net = tasks[first_task]['Networks'][0]
        except TypeError:
            # none port mappings at all (for e.g. for game-configuration)
            return {}
        reserved = first_net['ReservedPorts']
        dynamic = first_net['DynamicPorts']
        united = reserved or [] + dynamic or []
        return {x['Label']: x['Value'] for x in united}

    @staticmethod
    def get_events(elem):
        """Retrieve events from JSON response"""
        task_states = elem['TaskStates']
        if not task_states:
            return ""
        task_name = str(list(task_states.keys())[0])
        events = task_states[task_name]['Events']
        return events

    # When we need to send API down signals
    # downing or stopping task : Sent interrupt. (Killing)
    # unhealth check: healthcheck: check "check_api" unhealthy (Restart Signaled)

    # When we need to gather information about allocations:
    # Just after start
    # on event 'Task started by client' or Type='Started'

    async def setup_logger(self):
        (host, port) = self.logstash
        if not host or not port:
            return
        handler = await create_tcp_handler(host, int(port))
        root = logging.getLogger()
        root.setLevel(logging.DEBUG)
        root.addHandler(handler)
        root.info('Logger attached')
        self.log = root

    async def add_alloc_info(self, all_id: str) -> None:
        """Get and store additional information for specified allocation"""
        # Testing scenarios:
        #  all attempts are failed
        trials = 0
        while 'started' in self.allocs.get(all_id, {}):
            delay = self.req_retry + random.choice(range(10))
            print(f"Trying {trials} time for for alloc {all_id}. Sleep for "
                  f"{delay} sec")
            await asyncio.sleep(delay)
            trials += 1
            if trials > 10:
                print(f"{trials} times exceeded overall attempts. Exiting...")
                return

        if all_id not in self.allocs:
            self.allocs[all_id] = {
                'started': datetime.now() + timedelta(seconds=self.req_retry)
            }
            print("First time: ", all_id)
            self.allocs[all_id] = await self.get_allocation_ports(
                self.client, all_id)
        else:
            print("Data was retrieved already")

    async def logger(self):
        while True:
            await asyncio.sleep(10)
            print(self.allocs)

    async def process_events(self) -> None:
        """Processing events from Nomad in infinite loop"""

        index = None
        loop = asyncio.get_running_loop()
        await self.setup_logger()
        asyncio.create_task(self.logger())

        # disable timeouts
        timeout = aiohttp.ClientTimeout(total=0)
        self.client = aiohttp.ClientSession(loop=loop, timeout=timeout)

        first_req = True
        while True:
            resp, index = await self.nomad_req(self.client, 'allocations',
                                               index)
            new_ids = {x['ID']: self.get_events(x) for x in resp}
            for all_id, val in new_ids.items():
                for elem in val:
                    time = elem['Time']
                    if time not in self.events.keys():
                        self.events[time] = True
                        print(all_id, elem['Type'].ljust(20),
                              elem['DisplayMessage'])
                        if all_id not in self.allocs:
                            loop.create_task(self.add_alloc_info(all_id))
            if not first_req:
                # React to container crash
                pass
            first_req = False

    def run(self) -> None:
        """Run async loop"""
        asyncio.run(self.process_events())


def main():
    """Main entrypoint"""
    nomad_srv = os.getenv('NOMAD_ADDR', 'http://localhost:4646')
    logstash_srv = os.environ.get('GPAS_LOGSTASH_HOST')
    logstash_port = os.environ.get('GPAS_LOGSTASH_PORT')

    NomadListener(nomad_srv, (logstash_srv, logstash_port)).run()


if __name__ == '__main__':
    main()
